provider "azurerm" {
        subscription_id = 
        client_id = 
        client_secret = 
        tenant_id = 
        features {}
}
#virtual machine properties
variable "vmLinux" {}
variable "hostname" {}
variable "size" {}
variable "resourceGroupName" {}
variable "location" {}
variable "subnetID" {}
variable "imagePublisher"{}
variable "imageOffer" {}
variable "imageSKU" {}
variable "imageVersion" {}
variable "adminUsername" {}
variable "adminPassword" {}
#tags
variable "tagTechnicalOwner" {}
variable "tagBusinessOwner" {}
variable "tagBusinessService" {}
variable "tagCostCenter" {}
variable "tagDepartment" {}
variable "tagEnvironment" {}
variable "tagSubscription" {}
variable "tagApplication" {}
variable "tagShortDescription" {}
resource "azurerm_resource_group" "rg" {
    name     = var.resourceGroupName
    location = var.location
    tags = {
        "Technical Owner" = var.tagTechnicalOwner
        "Business Owner" = var.tagBusinessOwner
        "Business Service" = var.tagBusinessService
        "Cost Center" = var.tagCostCenter
        "Department" = var.tagDepartment
        "Environment" = var.tagEnvironment
        "Subscription" = var.tagSubscription
        "Application" = var.tagApplication
        "Short Description" = var.tagShortDescription
    }
}
resource "azurerm_network_interface" "nic" {
    name                        = join("-",[var.hostname,"nic","01"])
    location                    = var.location
    resource_group_name         = azurerm_resource_group.rg.name
    ip_configuration {
        name                          = join("-",[var.hostname,"nic","ipconfig"]    )
        subnet_id                     = var.subnetID
        private_ip_address_allocation = "Dynamic"
    }
    tags = {
        "Technical Owner" = var.tagTechnicalOwner
        "Business Owner" = var.tagBusinessOwner
        "Business Service" = var.tagBusinessService
        "Cost Center" = var.tagCostCenter
        "Department" = var.tagDepartment
        "Environment" = var.tagEnvironment
        "Subscription" = var.tagSubscription
        "Application" = var.tagApplication
        "Short Description" = var.tagShortDescription
    }
}
resource "azurerm_linux_virtual_machine" "vm" {
    count = var.vmLinux ? 1 : 0
    name                  = var.hostname
    location              = var.location
    resource_group_name   = var.resourceGroupName
    network_interface_ids = [azurerm_network_interface.nic.id]
    size                  = var.size
    os_disk {
        name              = join("",[var.hostname,"_OSDisk"])
        caching           = "ReadWrite"
        storage_account_type = "Premium_LRS"
    }
    source_image_reference {
        publisher = var.imagePublisher
        offer     = var.imageOffer
        sku       = var.imageSKU
        version   = var.imageVersion
    }
    computer_name  = var.hostname
    admin_username = var.adminUsername
    admin_password = var.adminPassword
    disable_password_authentication = false
    tags = {
        "Technical Owner" = var.tagTechnicalOwner
        "Business Owner" = var.tagBusinessOwner
        "Business Service" = var.tagBusinessService
        "Cost Center" = var.tagCostCenter
        "Department" = var.tagDepartment
        "Environment" = var.tagEnvironment
        "Subscription" = var.tagSubscription
        "Application" = var.tagApplication
        "Short Description" = var.tagShortDescription
    }
}
resource "azurerm_windows_virtual_machine" "vm" {
    count = var.vmLinux ? 0 : 1
    name                  = var.hostname
    location              = var.location
    resource_group_name   = var.resourceGroupName
    network_interface_ids = [azurerm_network_interface.nic.id]
    size                  = var.size
    os_disk {
        name              = join("",[var.hostname,"_OSDisk"])
        caching           = "ReadWrite"
        storage_account_type = "Premium_LRS"
    }
    source_image_reference {
        publisher = var.imagePublisher
        offer     = var.imageOffer
        sku       = var.imageSKU
        version   = var.imageVersion
    }
    computer_name  = var.hostname
    admin_username = var.adminUsername
    admin_password = var.adminPassword
    tags = {
        "Technical Owner" = var.tagTechnicalOwner
        "Business Owner" = var.tagBusinessOwner
        "Business Service" = var.tagBusinessService
        "Cost Center" = var.tagCostCenter
        "Department" = var.tagDepartment
        "Environment" = var.tagEnvironment
        "Subscription" = var.tagSubscription
        "Application" = var.tagApplication
        "Short Description" = var.tagShortDescription
    }
}